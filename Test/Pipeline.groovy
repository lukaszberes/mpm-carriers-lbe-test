pipelineJob("Test/Pipeline") {
	definition {
    cps {
      sandbox()
      script("""
        stage('HelloWorld') {
          build 'HelloWorld'
        } 
        stage('HelloWorld2') {
          build 'HelloWorld2'
        }
      """.stripIndent())      
    }
  }
}