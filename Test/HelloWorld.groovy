freeStyleJob("Test/HelloWorld") {

    displayName('Hello World')
    description('Hello world test')

    steps {
        shell("""
            #!/bin/bash
            echo Hello world !			
        """)
    }
}