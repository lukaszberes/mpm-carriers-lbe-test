freeStyleJob("HelloWorld2") {

    displayName('Hello World2')
    description('Hello world test2')

    steps {
        shell("""
            #!/bin/bash
			sleep 5s
            echo Hello world !			
        """)
    }
}