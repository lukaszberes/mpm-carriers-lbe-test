freeStyleJob("Jobs/A12_Test") {

    displayName('A12_Test')
    description('Run AllModules process')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2012')
  
    steps {
        batchFile ('''			
            @ECHO off
            SET SOAPUIPATH=c:\\Program Files\\SmartBear\\SoapUI-5.3.0\\bin
            SET PROJECTPATH=c:\\SoapUI Projects\\XSC\\All Modules
            SET RAPORTPATH=TestReports
            SET OUTPUTPATH=\\\\192.168.201.45\\tst.carrier_modules.SQL2008.mpm.metapack.com\\
            SET PROJECTCERTPATH=c:\\SoapUI Projects\\XSC\\Certification
            SET PROJECTPATHIABOL=c:\\SoapUI Projects\\XSC\\MPM-IABOL-INT

            set datecode=%date:~-4%%date:~4,2%%date:~7,2%_%time:~0,2%%time:~3,2%%time:~6,2%
            CALL SET dd=%datecode% %time%

            SET dd=%dd:~0,4%%dd:~5,2%%dd:~8,2%%dd:~11,2%%dd:~14,2%
            SET dd=%dd: =0%

            SET LOGFILE="%PROJECTPATH%\\Logs\\All Modules_%1_%dd%.log"

            FORFILES /s /p "%PROJECTPATH%\\Logs" /m "All Modules*.log" /c "cmd /c DEL @path" /d -7 >  %LOGFILE% 

            rem Usuwanie plików EDI
            DEL %OUTPUTPATH%\\edi /q

            rem Usuwanie plików labeli
            DEL %OUTPUTPATH%\\labels\\CarrierModules /q
            DEL %PROJECTPATH%\\Labels\\patterns\\*.* /q

            CD %PROJECTPATH%

            ECHO %USERDOMAIN%\\%USERNAME% >>  %LOGFILE% 
            ECHO %~dp0 >>  %LOGFILE% 

            ECHO Downloading latests items from TFS...
            CALL tf get "All Modules-soapui-project.xml" /force >>  %LOGFILE% 
            CALL tf get "SQL2008.prop" >>  %LOGFILE% 
            CALL tf get "All Modules-soapui-project" /recursive >>  %LOGFILE% 
            CALL tf get "Labels" /recursive >>  %LOGFILE% 
            CALL tf get "EDI" /recursive >>  %LOGFILE% 
            CALL tf get "../ScriptLibrary/*" >>  %LOGFILE%
            CALL tf get "../Certification/" /recursive >>  %LOGFILE%  
            CALL tf get "../MPM-IABOL-INT/" /recursive >>  %LOGFILE% 
            CALL tf get * /recursive >>  %LOGFILE% 

            ECHO Downloading latests items from TFS [DONE]

            SET JOBID=%RANDOM%

            for /f %%i in (\'more +2 "All Modules-SQL2008-EnvInfo.txt"\') do set AppVersion=%%i

            ECHO SoapUI running parameters:"
            ECHO   TestRunner        = "%SOAPUIPATH%\\testrunner.bat" 
            ECHO   jUnit report path = "%RAPORTPATH%\\%1" 
            ECHO   Properties file   = "%1.prop" 
            ECHO   Report job ID     = "%JOBID%"
            ECHO   Project file      = "%PROJECTPATH%\\All Modules-soapui-project.xml"
            ECHO   Log file          = "%LOGFILE%"
            ECHO   AppVersion        = %AppVersion% 

            rem uruchomienie testu dla kontraktów od A do H
            ECHO Run tests for A-H directory
            ECHO CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f"%RAPORTPATH%\\%1" -Dsoapui.properties.AllModules=%1.prop -PDbLogJobID="%JOBID%" -PTestSubfolder="A-H" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATH%\\All Modules-soapui-project.xml" >>  %LOGFILE% 
            CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f"%RAPORTPATH%\\%1" -Dsoapui.properties.AllModules=%1.prop -PDbLogJobID="%JOBID%" -PTestSubfolder="A-H" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATH%\\All Modules-soapui-project.xml"  >>  %LOGFILE% 
            ECHO Run tests for A-H directory [DONE]

            rem uruchomienie testudla kontraktów od I do Z
            ECHO Run tests for I-Z directory
            ECHO CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f"%RAPORTPATH%\\%1" -Dsoapui.properties.AllModules=%1.prop -PDbLogJobID="%JOBID%" -PTestSubfolder="I-Z" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATH%\\All Modules-soapui-project.xml" >>  %LOGFILE% 
            CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f"%RAPORTPATH%\\%1" -Dsoapui.properties.AllModules=%1.prop -PDbLogJobID="%JOBID%" -PTestSubfolder="I-Z" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATH%\\All Modules-soapui-project.xml"  >>  %LOGFILE% 
            ECHO Run tests for I-Z directory [DONE]

            rem uruchomienie kontraktoów Certefication
            ECHO Run tests for Certefication directory
            echo CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f%RAPORTPATH%\\%1 -Dsoapui.properties.Certification=Certification.prop -PDbLogJobID="%JOBID%" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTCERTPATH%\\Certification-soapui-project.xml" >>  %LOGFILE%

            CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f%RAPORTPATH%\\%1 -Dsoapui.properties.Certification=Certification.prop -PDbLogJobID="%JOBID%" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTCERTPATH%\\Certification-soapui-project.xml" >>  %LOGFILE%
            ECHO Run tests for Certefication directory [DONE]

            rem uruchomienie kontraktów MPM-IABOL-INT
            ECHO Run tests for MPM-IABOL-INT directory
            echo CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f%RAPORTPATH%\\%1 -Dsoapui.properties.MPM-IABOL-INT=MPM-IABOL-INT.prop -PDbLogJobID="%JOBID%" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATHIABOL%\\MPM-IABOL-INT-soapui-project.xml" >>  %LOGFILE%

            CALL "%SOAPUIPATH%\\testrunner.bat" -j -J -f%RAPORTPATH%\\%1 -Dsoapui.properties.MPM-IABOL-INT=MPM-IABOL-INT.prop -PDbLogJobID="%JOBID%" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATHIABOL%\\MPM-IABOL-INT-soapui-project.xml" >>  %LOGFILE%
            ECHO Run tests for MPM-IABOL-INT directory [DONE]

            '''.stripIndent())
    }
}