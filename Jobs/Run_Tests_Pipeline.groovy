pipelineJob("Pipelines/Run Tests") {
    properties {
        disableConcurrentBuilds()
        disableResume() 
    }
    triggers {
        cron("0 0 * * 2-4 \n 0 0 7-13,24-30 * 5 \n 0 1 * * 1")
    } 
    definition {
        cps {
            sandbox()
            script("""
                    stages {
                        stage ('Restoring Database') {
                            steps {
                                build job: 'jobs/carrier-modules/01.DB_Restore'
                            }
                        }
                        stage ('Restoring Application') {
                            parallel{
                                stage('Restoring Application') {
                                    steps {
                                        build job: 'jobs/carrier-modules/02.APP_Restore'
                                    }
                               }
                               stage('System Data Update') {
                                    steps {
                                        build job: 'jobs/carrier-modules/03.System_Data_Update'
                                    }
                               }
                            }
                        }
                        stage ('Application install') {
	                	    steps {
	                		    build job: 'jobs/carrier-modules/04.APP_Install'
	                	    }
                        }
                        stage ('Importing Contracts') {
                            parallel{
                                stage('Application backup') {
                    				steps {
                    					build job: 'jobs/carrier-modules/05.APP_Backup'
                    				}
                               }
                                stage('Importing Contracts') {
                    				steps {
                    					build job: 'jobs/carrier-modules/06.CM_Import'
                    				}
                                }
                            }
                        }
                        stage('Import DEV Routing codes') {
                    		steps {
                    			build job: 'jobs/carrier-modules/08.TR_RC_DEV_Import'
                    		}
                        }
                        stage ('Start Guard Service') {
                    		steps {
                    			build job: 'jobs/carrier-modules/10.Guard_Start'
                    		}
                        }
                        stage ('Run SoapUI Tests') {
                            parallel{
                                stage('Run SoapUI Tests') {
                    				steps {
                    					build job: 'jobs/carrier-modules/12.Test'
                    				}
                                }
	                			stage('Run Sequences Tests') {
	                				steps {
                    					build job: 'jobs/carrier-modules/15.Sequences',
	                					parameters: [
	                						string(name: 'REPORT_ID', value: UUID.randomUUID().toString())
	                					]
                    				}
                                }
                            }
                       }
                       stage ('Compare Labels & EDI') {
                            parallel{
                                stage('Compare EDI') {
                    				steps {
                    					build job: 'jobs/carrier-modules/13.Compare_EDI'
                    				}
                               }
                                stage('Compare Labels') {
                    				steps {
                    					build job: 'jobs/carrier-modules/14.Compare_Labels'
                    				}
                                }
                            }
                       }
                    }
                """.stripIndent())     
        }
    }
}