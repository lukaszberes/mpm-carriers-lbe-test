freeStyleJob("Jobs/A09_ZIP_DB_Backup") {

    displayName('A09_ZIP_DB_Backup')
    description('ZIP DB Backup')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            $cfg_repoPath = "C:/BACKUP/Carrier_Modules/Published/"
			$cfg_testRepoPath = "C:/BACKUP/Carrier_Modules/Test/"
			$cfg_remoteRepoPath = "//192.168.200.87/XSC_Backup/AllModules/SQL2008/Database/"
			
			function compressFiles( $path, $extension ) {
			
				$files = Get-ChildItem $path -Filter $extension | sort Name -Descending |select name
				
				if ($files -ne $null)
				{
					foreach ( $file in $files )
					{
						$fileName = $file.Name
						
						$zipFileName = $fileName.Substring( 0, $fileName.LastIndexOf(".") ) + ".zip"
						$zipFilePath = $path + $zipFileName 
						
						cd  $path
						zip $zipFilePath $fileName
						
						Write-Output "[INFO]: \$(Get-Date -format 'g') -> Compressing $fileName in \$path" >> $cfg_logPath
						
						Copy-Item $zipFilePath -Destination $cfg_remoteRepoPath
						
						$fileToRemove = $path + $fileName
						Remove-Item $fileToRemove
					}
				}
				else 
				{
					Write-Output "[INFO]: \$(Get-Date -format 'g') -> There is no *.bak files in \$path" >> $cfg_logPath
				}
			}
			
			compressFiles -path $cfg_repoPath -extension "*.bak"
			compressFiles -path $cfg_testRepoPath -extension "*.bak"

            '''.stripIndent())
    }
}