freeStyleJob("Jobs/A10_Guard_Start") {

    displayName('A10_Guard_Start')
    description('Starts the Guard service')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            $environment = "SQL2008"
            $cfg_XGuardServiceName = "GuardService_$environment"
            $cfg_setGuardStatus = "Start" #Start, Stop, Restart

            [Environment]::UserName

            #Start Guard
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> Starting XSCGuard: \$cfg_XGuardServiceName" > $cfg_logPath

            try
            {
                switch ( $cfg_setGuardStatus )
                {
                    "Start"
                    {
                        $code = [scriptblock]::Create("Start-Service \$cfg_XGuardServiceName")
                    }
                    "Stop"
                    {
                        $code = [scriptblock]::Create("Stop-Service \$cfg_XGuardServiceName")
                    }
                    "Restart"
                    {
                        $code = [scriptblock]::Create("Restart-Service \$cfg_XGuardServiceName")
                    }
                }

                # zatrzymanie uslugi jako Administrator
                # polecenie zatrzymania uslugi zdefiniowane jako scriptblock powyzej  
                Start-Process -FilePath powershell.exe -ArgumentList $code -verb RunAs
            }
            catch [Exception]
            {
                Write-Output "[ERR ]: \$(Get-Date -format 'g') -> Starting XSCGuard failed: \$_.Exception.Message " > $cfg_logPath
                exit 9
            }

            Write-Output "[INFO]: \$(Get-Date -format 'g') -> XSCGuard: \$cfg_XGuardServiceName started" >> $cfg_logPath

            '''.stripIndent())
    }
}