freeStyleJob("Jobs/A05_APP_Backup") {

    displayName('A05_APP_Backup')
    description('')

	logRotator {
        daysToKeep(10)
    }
	
    label('APP_SQL_TEST')
  
     steps {
        powerShell ('''			
            $jobName = $args[0] #"SMR_SQL2008_APP_Backup" 
            $environment = $args[1] #"SMR" 

            #DEBUG ON or OFF
            $DebugPreference = "SilentlyContinue" # "Continue" or "SilentlyContinue"
            $importSuccessfuly = $false

            # zmienne z prefiksem cfg_ sa zdefiniowane w c:\\jenkins\\script\\All_Modules_Config.ps1
            # sa do zmienne do wykorzystania w wielu skryptach, ale w kazdym mozna je nadpisac
            . c:\\programs\\jenkins\\script\\All_Modules_Config.ps1 $jobName $environment #"Test" "SQL2008" # 
            . c:\\programs\\jenkins\\script\\Library\\Calendar_functions.ps1
            . c:\\programs\\jenkins\\script\\Library\\Tools_functions.ps1


            $cfg_XSCInstallPath = $env:PATH_MPM_INSTALL
            $cfg_repoPath = $env:PATH_APP_BACKUP

            if ( ! ( Test-Path $cfg_logDir ) ) { mkdir $cfg_logDir }

            Write-Output "[INFO]: $(Get-Date -format 'g') -> Start application backup" > $cfg_logPath

            $installedVersion = getInstalledVersion -path $cfg_XSCInstallPath
            Write-Output "[INFO]: $(Get-Date -format 'g') -> Installed XSC version is: $installedVersion" >> $cfg_logPath

            <#if ( isEndOfSprint ) {
            
                if (Test-Path $cfg_newPubVersionPath)
                {
                
                    # getLastXSCVersion() is function in Tools_functions.ps1
                    $lastPubVersion = getLastXSCVersionFullName -path $cfg_newPubVersionPath
                    Write-Output "[INFO]: $(Get-Date -format 'g') -> Last published XSC version is: $lastPubVersion" >> $cfg_logPath
                }
                else
                {
                    Write-Output "[ERR ]: $(Get-Date -format 'g') -> Missing $cfg_newVersionFormat or $cfg_oldVersionFormat directories" >> $cfg_logPath
                    exit 9
                }
                $installedVersion = $lastPubVersion

            }
            else {
            
                if (Test-Path $cfg_newDevVersionPath)
                {
                
                    # getLastXSCVersion() is function in Tools_functions.ps1
                    $lastDevVersion = getLastXSCVersionFullName -path $cfg_newDevVersionPath
                    Write-Output "[INFO]: $(Get-Date -format 'g') -> Last published development XSC version is: $lastDevVersion" >> $cfg_logPath
                }
                else
                {
                
                    Write-Output "[ERR ]: $(Get-Date -format 'g') -> Missing $cfg_newVersionFormat or $cfg_oldVersionFormat directories" >> $cfg_logPath
                    exit 9
                }
                $installedVersion = $lastDevVersion
            }#>

            $XSCLocalUpdatePath = $cfg_XSCInstallPath + "Data\\Updates\\"

            #czyszczenie katalogów w celu mniejszego zip-a
            $XSCLocalUpdateFilePath = $XSCLocalUpdatePath + "*.*"
            Write-Output "[INFO]: $(Get-Date -format 'g') -> Cleaning $XSCLocalUpdateFilePath" >> $cfg_logPath
            Remove-Item $XSCLocalUpdateFilePath

            $backupsPath = $XSCLocalUpdatePath + "Backup\\*" 
            Write-Output "[INFO]: $(Get-Date -format 'g') -> Cleaning $backupsPath" >> $cfg_logPath
            Remove-Item -Path $backupsPath -Recurse

            $installedPath = $XSCLocalUpdatePath + "Installed\\*" 
            Write-Output "[INFO]: $(Get-Date -format 'g') -> Cleaning $installedPath" >> $cfg_logPath
            Remove-Item -Path $installedPath -Recurse

            $omittedPath = $XSCLocalUpdatePath + "Omitted\\*" 
            Write-Output "[INFO]: $(Get-Date -format 'g') -> Cleaning $omittedPath" >> $cfg_logPath
            Remove-Item -Path $omittedPath -Recurse

            #$logsPath = $cfg_XSCInstallPath + "Logs\\*" 
            #Write-Output "[INFO]: $(Get-Date -format 'g') -> Cleaning $logsPath" >> $cfg_logPath
            #Remove-Item -Path $logsPath -Recurse

            #zzipowanie aplikacji
            Write-Output "[INFO]: $(Get-Date -format 'g') -> Creating XSC aplication backup for $installedVersion version" >> $cfg_logPath
            $zipCmdParamters = $cfg_repoPath + $installedVersion.Substring( $installedVersion.IndexOf("\\") + 1 ) + ".zip"
            Write-Output "[INFO]: $(Get-Date -format 'g') -> $zipCmdParamters " >> $cfg_logPath

            & zip -r $zipCmdParamters $cfg_XSCInstallPath

            Write-Output "[INFO]: $(Get-Date -format 'g') -> End application backup" >> $cfg_logPath

            '''.stripIndent())
    }
}