freeStyleJob("Jobs/A07a_DB_Backup") {

    displayName('A07a_DB_Backup')
    description('')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2008')
  
    steps {
        batchFile 'powershell -ExecutionPolicy Bypass -File c:\\jenkins\\script\\DBBackup\\DB_Backup_4RC.ps1 %JOB_NAME% SQL2008'
	}
}