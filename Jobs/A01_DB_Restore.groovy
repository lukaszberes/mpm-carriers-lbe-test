freeStyleJob("Jobs/A01_DB_Restore") {

    displayName('A01_DB_Restore')
    description('Restore of the database')

	logRotator {
        daysToKeep(30)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            [Environment]::Is64BitProcess
            [Environment]::UserName 
            Get-PSSnapin -Registered

            Add-Type -AssemblyName System.IO.Compression.FileSystem
            Add-PSSnapin SqlServerCmdletSnapin100
            Add-PSSnapin SqlServerProviderSnapin100

            $cfg_repoPath = $env:PATH_DB_BACKUP
            $lastInstalledVersion = $env:LAST_INSTALLED_VERSION
            $cfg_server = $env:SERVER_IP
            $cfg_instance = $env:INSTANCE 
            $cfg_database = $env:DATABASE_NAME

            Write-Output "Starting restore procedure with parameters:"
            Write-Output "Path database backup from: $cfg_repoPath"
            Write-Output "Version restroed: $lastInstalledVersion"
            Write-Output "Server IP: $cfg_server"
            Write-Output "Instance: $cfg_instance"
            Write-Output "Database name: $cfg_database"


            #Functions

            function Unzip {
                param([string]$zipfile, [string]$outpath)

                [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
            }

            function getLastBackup( $path, $version ) {
            
                $dbBackupFullPath = $null

                $getLastBackup = Get-ChildItem $path -Filter *$version.bak | sort Name -desc | select Name -First 1

                if ( $getLastBackup -ne $null ) {
                
                    $getLastBackup = $getLastBackup.name

                    $dbBackupFullPath = $path + $getLastBackup
                }
                else {
                
                    $getLastBackup = Get-ChildItem $path -Filter *$version.zip | sort Name -desc | select Name -First 1

                    if ( $getLastBackup -ne $null ) {
                    
                        $getLastBackup = $getLastBackup.name

                        $dbBackupFullPath = $path + $getLastBackup

                        Write-Output "[INFO]: $(Get-Date -format 'g') -> Unzip backup: $dbBackupFullPath" >> $cfg_logPath
                        Unzip $dbBackupFullPath $path
                        Write-Output "[INFO]: $(Get-Date -format 'g') -> Unzip done" >> $cfg_logPath

                        $dbBackupFullPath = getLastBackup -path $path -version $version
                    }

                }
                return $dbBackupFullPath
            }

            function restore( $server, $instance, $database, $backupFileName ) {
            
                $DBParam1 = "database=" + $database
                $DBParam2 = "backupFileName=" + $backupFileName

                $DBParams = $DBParam1, $DBParam2

                try {
                    $error.Clear()
                    $result = Invoke-Sqlcmd -InputFile 'c:\\jenkins\\script\\DBRestore\\DB_restore.sql\' -Variable $DBParams -ServerInstance $server\\$instance -QueryTimeout 65535 -OutputSqlErrors $true -AbortOnError -SeverityLevel 0 -ErrorLevel 0 -Verbose
                } 
                catch [Exception] {
                    $result = $error 
                }
                return $result
            }

            Write-Output "[INFO]: $(Get-Date -format 'g') -> Starting database restore: $backupFullPathName" >> $cfg_logPath

            $backupFullPathName = getLastBackup -path $cfg_repoPath -version $lastInstalledVersion

            # Restore database
            Write-Output "Ready to perform restore..."
            $restoreResult = restore -server $cfg_server -instance $cfg_instance -database $cfg_database -backupFileName $backupFullPathName
            Write-Output "Restored: $restoreResult"

            Remove-Item $backupFullPathName

            #printRestoreResults( $restoreResult )

            Write-Output "[INFO]: $(Get-Date -format 'g') -> End database restore" >> $cfg_logPath

            '''.stripIndent())
    }
}