freeStyleJob("Jobs/A15_Sequences") {

    displayName('A15_Sequences')
    description('Run Sequences tests process')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            @ECHO off

            SET SOAPUIPATH=c:\\Program Files\\SmartBear\\SoapUI-5.3.0\\bin
            SET WORKDIR=c:\\SoapUI Projects\\XSC\\Sequences
            SET RAPORTPATH=TestReports
            SET PROJECTPATHSEQUENCES=C:\\SoapUI Projects\\XSC\\All Modules\\All Modules-soapui-project\\Playground\\Sequences
            
            SET LOGFILE="%WORKDIR%\\Logs\\Sequences_%BUILD_ID%.log"
            
            FORFILES /s /p "%WORKDIR%\\Logs" /m "Sequences*.log" /c "cmd /c DEL @path" /d -7 >  %LOGFILE% 
            
            CD %WORKDIR%
            
            ECHO %USERDOMAIN%\\%USERNAME% >>  %LOGFILE% 
            ECHO %~dp0 >>  %LOGFILE% 
            
            ECHO Downloading latests items from TFS...
            CALL tf get "All Modules-soapui-project" /recursive >>  %LOGFILE% 
            CALL tf get * /recursive >>  %LOGFILE% 
            
            ECHO Downloading latests items from TFS [DONE]
            
            for /f %%i in (\'more +2 "C:\\SoapUI Projects\\XSC\\All Modules\\All Modules-SQL2008-EnvInfo.txt"\') do set appVersion=%%i
            
            ECHO SoapUI running parameters:"
            ECHO   TestRunner			= %SOAPUIPATH%\\testrunner.bat 
            ECHO   jUnit report path		= %RAPORTPATH%\\SEQUENCES
            ECHO   Properties file		= SEQUENCES.prop 
            ECHO   Job ID			= %REPORT_ID%
            ECHO   Project file			= %PROJECTPATHSEQUENCES%\\Squences-soapui-project.xml
            ECHO   Log file			= %LOGFILE%
            ECHO   AppVersion			= %appVersion%
            
            rem uruchomienie kontraktów Sequences
            ECHO Run tests for SEQUENCES directory
            echo CALL "%SOAPUIPATH%\\testrunner.bat" -Dsoapui.properties.AllModules=SEQUENCES.prop -PDbLogJobID="%REPORT_ID%" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATHSEQUENCES%\\Squences-soapui-project.xml" >>  %LOGFILE%
            
            CALL "%SOAPUIPATH%\\testrunner.bat" -Dsoapui.properties.AllModules=SEQUENCES.prop -PDbLogJobID="%REPORT_ID%" -PDbLogEnabled=True -PDbLogConnectionString="jdbc:mysql://192.168.200.224:3306/allmodules?user=allmodules&password=allmodules" "%PROJECTPATHSEQUENCES%\\Squences-soapui-project.xml" >>  %LOGFILE%
            
            IF errorlevel -1 (
               ECHO [DONE] Run tests for SEQUENCES directory : FAILED
            )
            IF errorlevel 0 (
            	ECHO [DONE] Run tests for SEQUENCES directory : OK
            )
            
            EXIT 0

            '''.stripIndent())
    }
}