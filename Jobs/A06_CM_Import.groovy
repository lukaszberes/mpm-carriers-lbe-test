freeStyleJob("Jobs/A06_CM_Import") {

    displayName('A06_CM_Import')
    description('Importing Carrier Modules')

	logRotator {
        daysToKeep(30)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            $cfg_XSCInstallPath = $env:XSC_INSTALL_PATH
            $cfg_ImportFilePath = $env:IMPORT_FILE_PATH
            $cfg_XSCPassword = $env:XSC_PASSWORD
            $cfg_XSCUser = $env:XSC_USER
            $cfg_XSCUrl = $env:XSC_URL
            $cfg_server = $env:SERVER_DB
            $cfg_instance = $env:INSTANCE
            $cfg_database = $env:DATABASE_NAME
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import contract configuration" >> $cfg_logPath
            importConfiguration
            
            function importConfiguration()
            {
                Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import COnfiguration" 
            
                $dataImportPath = $cfg_XSCInstallPath + "Data/ClientsData/XSMS/bin"
            
                $getImportDirectories = Get-ChildItem $cfg_ImportFilePath* -Directory | sort Name -desc #| select Name -First 1
                # w pętli
                foreach ( $dir in $getImportDirectories )
                {
                    $path = $dir.FullName + "/"
                    $getImportFileNames = Get-ChildItem $path* -Include *.xscsys, *.xml, *kfi  | sort Name -desc 
                    foreach ( $file in $getImportFileNames )
                    {
                        $unitName = $dir.name
                        $fileName = $file.name 
                        $fileFullName = $file.fullname
                        
                        import -dataImportPath $dataImportPath -unitName $unitName -fileFullName $fileFullName 
                    }
                }
            }
            
            # Do prawidlowego dzialania do katalogu Data/ClientsData/XSMS/bin trzeba doinstalowac DataImport.exe oraz NLog oraz FluentCommandLineParser oraz Helpers oraz ExportSupport
            function import( $dataImportPath, $unitName, $fileFullName )
            {
                Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import" >> $cfg_logPath
            
                Write-Output "\$dataImportPath/DataImport -d \$unitName -a \$cfg_XSCUrl/blackbox/shippingmanagement.svc -f \$fileFullName -p \$cfg_XSCPassword -u \$cfg_XSCUser"
                & $dataImportPath/DataImport -d $unitName -a $cfg_XSCUrl/blackbox/shippingmanagement.svc -f $fileFullName -p $cfg_XSCPassword -u $cfg_XSCUser
            
                # odczytanie kodu
                # jeśli OK, to przeniesienie zaimportowanych plików do folderu Done + dodanie do nazwy pliku daty importu
                if ( $LASTEXITCODE -eq 0 ) 
                {
                    $importSuccessfuly = $true
            
                    $destinationPath = $path + "Done/" + $(Get-Date -format yyyyMMdd%Hmm) + "_" + $file.name
                        
                    # dodanie wpisu do logu co zostało zaimportowane
                    Write-Output "[INFO]: \$(Get-Date -format 'g') -> Imported $file" >> $cfg_logPath
                }
                else 
                {
                    $destinationPath = $path + "Failed/" + $(Get-Date -format yyyyMMdd%Hmm) + "_" + $file.name
                        
                    # dodanie wpisu do logu co zostało NIE zaimportowane
                    Write-Output "[ERR ]: \$(Get-Date -format 'g') -> Import $file failed" >> $cfg_logPath
                }
            }
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> Contract activation" >> $cfg_logPath
            
            $result = CM_activate
            
            function CM_activate
            {
                    
                $activatedContracts = @()
                $failedContracts = @()
            
                $fileName = "ModulesActivation.txt"
                $fileFullPath = $cfg_ImportFilePath + $fileName
            
                # Create the connection string
                $sqlConnectionString ="Server = \$cfg_server/\$cfg_instance; Database = \$cfg_database; Integrated Security = True"
                $sqlConnection = New-Object System.Data.SqlClient.SqlConnection
                $sqlConnection.ConnectionString = $sqlConnectionString
            
                #Create the SQL Command object
                $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
                $sqlCmd.Connection = $SqlConnection
            
                $XLIdentifiers = Import-Csv -Delimiter ";" -Path $fileFullPath
                
                foreach ( $row in $XLIdentifiers )
                {
            
                    Write-Host $row
            
                    if ( ($row.CONTRACTTYPE_XLIDENTIFIER.StartsWith("//")) -or ($row.CONTRACTTYPE_XLIDENTIFIER -eq "") )
                    {
                        continue
                    }
            
                    $moduleActivationKey = $row.MODULE_ACTIVATION_KEY
                    $contractTypeXLIdentifier = $row.CONTRACTTYPE_XLIDENTIFIER
            
                    $sqlQuery = "UPDATE [\$cfg_database].[dbo].[TB_EVLS_MODULE] set MODULE_ACTIVATION_KEY='$moduleActivationKey', Module_state=7 where [MODULE_ID] = (select [CONTRACTTYPE_MODULE_ID] from [TB_EVLS_CONTRACTTYPE] where [CONTRACTTYPE_XLIDENTIFIER] = '\$contractTypeXLIdentifier')"
                    $sqlCmd.CommandText = $sqlQuery
            
                    #Open SQL connection
                    $sqlCmd.Connection.Open()
            
                    #Execute the Query
                    $sqlResult = $sqlCmd.ExecuteNonQuery()
            
                    $sqlCmd.Connection.Close()
            
                    $processedContracts = '"' + $contractTypeXLIdentifier + '";"' + $moduleActivationKey + '"'
                    
                    if ( $sqlResult -gt 0 )
                    {
                        Write-Output "[INFO]: \$(Get-Date -format 'g') -> Contract \$contractTypeXLIdentifier activated" >> $cfg_logPath
                        $activatedContracts += $processedContracts 
                        $result = $result + $sqlResult
                    }
                    else
                    {
                        Write-Output "[ERR ]: \$(Get-Date -format 'g') -> Contract \$contractTypeXLIdentifier NOT activated" >> $cfg_logPath
                        $failedContracts += $processedContracts 
                    }
            
                }
            
                    if ( $failedContracts.count -ne 0 )
                    {
                        $destinationPath = $cfg_ImportFilePath + "Failed/" + $(Get-Date -format yyyyMMdd%Hmm) + "_" + $fileName
            
                        foreach ( $row in $failedContracts) 
                        {
                            Write-Output $row >> $destinationPath
                        }
                    }
                        
                    #Move-Item -path $fileFullPath -destination $destinationPath 
            
                    Write-Output 'CONTRACTTYPE_XLIDENTIFIER;MODULE_ACTIVATION_KEY' > $fileFullPath
                    Write-Output '//example: "DHL-12";"G95UPPPPP93KPUP5PPP8PPPPPPPP9PPPP2QQQQQQQQQQQQQQXEQWQ;323873434368564E506B553D"' >> $fileFullPath
            
                }
            
                return $result
            }
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> \$result contract(s) was activated" >> $cfg_logPath
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> End CM import" >> $cfg_logPath

            '''.stripIndent())
    }
}