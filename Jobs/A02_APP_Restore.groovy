freeStyleJob("Jobs/A02_APP_Restore") {

    displayName('A02_APP_Restore')
    description('Restore of the App')

	logRotator {
        daysToKeep(30)
    }
	
    label('DB_SQL2012')
  
   steps {
        powerShell ('''			
            $jobName = $args[0]
            $environment = $args[1]
            $environmentKind = $args[2]

            $cfg_XSCInstallPath = $env:PATH_MPM_INSTALL
            $cfg_repoPath = $env:PATH_APP_BACKUP
            $previousInstalledPubVersion = $env:LAST_INSTALLED_VERSION
            $cfg_XGuardServiceName = "GuardService"
            $cfg_XSCDrive = "d:\\"

            #DEBUG ON or OFF
            $DebugPreference = "SilentlyContinue" # "Continue" or "SilentlyContinue"
            $importSuccessfuly = $false
            $run = $true

            # zmienne z prefiksem cfg_ sa zdefiniowane w c:\\programs\\jenkins\\script\\All_Modules_Config.ps1
            # są to zmienne do wykorzystania w wielu skryptach, ale w każdym można je nadpisać
            #. c:\\programs\\jenkins\\script\\All_Modules_Config.ps1 $jobName $environment $environmentKind #"Test" "SQL2008"
            #. c:\\programs\\jenkins\\script\\Library\\Calendar_functions.ps1
            #. c:\\programs\\jenkins\\script\\Library\\Tools_functions.ps1

            #Functions
            function getInstalledVersion( $server, $instance, $database ) 
            {
                $getInstalledVersion = invoke-sqlcmd -Query "SELECT SYSTEM_VERSION FROM [$database].[dbo].[TB_EVLS_SYSTEM];" -ServerInstance $server\\$instance -QueryTimeout 0 
                $installedVersion = $getInstalledVersion.SYSTEM_VERSION

                return $installedVersion
            }

            function getAppBackup( $path, $version )
            {
                $getAppBackup = Get-ChildItem $path -Filter *$version.zip | sort Name -desc | select Name -First 1
                $appBackup = $getAppBackup.name

                return $appBackup
            }

                # Kill DeploymentTool
                Write-Output "[INFO]: $(Get-Date -format 'g') -> Kill DeploymentTool"
            	Write-Output "[INFO]: $(Get-Date -format 'g') -> Kill DeploymentTool" >> $cfg_logPath

            	# http://www.ravichaganti.com/blog/wmi-query-language-wql-keywords-and-operators/
            	# https://msdn.microsoft.com/en-us/library/aa394606(v=vs.85).aspx
            	Get-WmiObject Win32_Process -Filter "name like 'MPMDeploymentTool%'" | ForEach-Object { $_.Terminate().ReturnValue }

            	Stop-Process -Name MPMDeploymentTool* -Force -Verbose

                # Stop Guard
                Write-Output "[INFO]: $(Get-Date -format 'g') -> Stopping XSCGuard"
            	Write-Output "[INFO]: $(Get-Date -format 'g') -> Stopping XSCGuard" >> $cfg_logPath

                $code = [scriptblock]::Create("Get-Service $cfg_XGuardServiceName | Where {$_.status -eq 'running'}  | Stop-Service -PassThru")

                Write-Output "[INFO]: $(Get-Date -format 'g') -> code: $code"
                # zatrzymanie usługi jako Administrator
                # polecenie zatrzymania usługi zdefiniowane jako scriptblock powyżej

                $process = Start-Process -FilePath powershell.exe -ArgumentList $code -verb RunAs
                Write-Output "[INFO]: $(Get-Date -format 'g') ->  $process"
                Write-Output "[INFO]: $(Get-Date -format 'g') ->  $process" >> $cfg_logPath

                #Stop-Service $cfg_XGuardServiceName

            	# odczytanie informacji jak wersja publiczna byla ostatnio instalowana
            	# findStringInFile() is function in Tools_functions.ps1


            	# odczytanie jaka aktualnie wersja jest zainstalowana
            	#$previousInstalledVersion = getInstalledVersion -path $cfg_XSCInstallPath

            	# pobranie backupu
            	$appBackup = getAppBackup -path $cfg_repoPath -version $previousInstalledPubVersion


                    # usuniecie calego XSC poprzednie wersje
                    Write-Output "[INFO]: $(Get-Date -format 'g') -> Removing previous version of XSC: $previousInstalledVersion"
            		Write-Output "[INFO]: $(Get-Date -format 'g') -> Removing previous version of XSC: $previousInstalledVersion" >> $cfg_logPath
            		$tmpXSCInstallPath = $cfg_XSCInstallPath + "*"
            		Remove-Item -Path $tmpXSCInstallPath -Recurse -Force

                    # rozpakowanie poprzedniej wersji aplikacji z repo
                    $appBackupFullPath = $cfg_repoPath + $appBackup
                    Write-Output "[INFO]: $(Get-Date -format 'g') -> Restore previous publish version: $previousInstalledPubVersion from $appBackupFullPath"
            		Write-Output "[INFO]: $(Get-Date -format 'g') -> Restore previous publish version: $previousInstalledPubVersion from $appBackupFullPath" >> $cfg_logPath
            		#$appBackupFullPath = $cfg_repoPath + "\\" + $appBackup

                    #Unzip $appBackupFullPath "d:\\"
                    try {
                        $error.Clear()
                        $error = & unzip $appBackupFullPath -d $cfg_XSCDrive
                    }
                    catch [Exception] {
                        $result = $error 
                    }
                    return $result

                    Write-Output "[INFO]: $(Get-Date -format 'g') -> Errrrroooooor: $result"

                    <#if ( $LASTEXITCODE -ne 0 ) 
                    {
            		    Write-Output "[INFO]: $(Get-Date -format 'g') -> Last extocode from UNZIP.exe + $LASTEXITCODE ." >> $cfg_logPath
                        exit 9
                    }#>

                Write-Output "[INFO]: $(Get-Date -format 'g') -> End application restore"
            	Write-Output "[INFO]: $(Get-Date -format 'g') -> End application restore" >> $cfg_logPath

            '''.stripIndent())
    }
}