freeStyleJob("Jobs/A01a_DB_Restore") {

    displayName('A01a_DB_Restore')
    description('Restore of the database on weekend')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2008')
  
    steps {
        batchFile 'powershell -ExecutionPolicy Bypass -File c:\\jenkins\\script\\DBRestore\\DB_Restore_4RC.ps1 %JOB_NAME% SQL2008'
	}
}