freeStyleJob("Jobs/A14_Compare_Labels") {

    displayName('A14_Compare_Labels')
    description('Run Labels comparator')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2012')
  
    steps {
        batchFile ('''			
            @ECHO off

            CALL SET dd=%date% %time%

            SET dd="%dd:~0,4%%dd:~5,2%%dd:~8,2%%dd:~11,2%%dd:~14,2%"
            SET dd=%dd: =0%

            rem SET SOAPUIPATH=c:\\Program Files\\SmartBear\\SoapUI-Pro-5.1.2\\bin
            SET SOAPUIPATH=c:\\Program Files\\SmartBear\\SoapUI-5.2.1\\bin 
            SET PROJECTPATH=c:\\SoapUI Projects\\XSC\\All Modules
            SET RAPORTPATH=TestReports

            CD %PROJECTPATH%
            ECHO %PATH%

            CALL tf get "Labels" /recursive

            rem uruchomienie porównania lebeli 
            echo CALL "%SOAPUIPATH%\\testrunner.bat" -Dsoapui.properties.AllModules=%1.prop -PLabelsComparison=true -s"Compare labels" -f%RAPORTPATH%\\%1 "%PROJECTPATH%\\All Modules-soapui-project.xml"
            CALL "%SOAPUIPATH%\\testrunner.bat" -Dsoapui.properties.AllModules=%1.prop -PLabelsComparison=true -s"Compare labels" -f%RAPORTPATH%\\%1 "%PROJECTPATH%\\All Modules-soapui-project.xml" > "%PROJECTPATH%\\Logs\\All Modules_Compare_Labels_%1_%dd%.log"

            '''.stripIndent())
    }
}