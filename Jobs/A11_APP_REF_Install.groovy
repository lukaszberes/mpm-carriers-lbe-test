freeStyleJob("Jobs/A11_APP_REF_Install") {

    displayName('A11_APP_REF_Install')
    description('')

	logRotator {
        daysToKeep(30)
    }
	
    label('APP_SQL_TEST')
  
    steps {
        batchFile 'powershell -ExecutionPolicy Bypass -File c:\\programs\\jenkins\\script\\AppInstall\\App_REF_Install.ps1 %JOB_NAME% SQL2008'
	}
}