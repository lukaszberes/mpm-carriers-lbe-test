freeStyleJob("Jobs/A08a_TR_RC_PROD") {

    displayName('A08a_TR_RC_PROD')
    description('Importing PROD Triggers for Routing Codes')

	logRotator {
        daysToKeep(30)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            $forceDEV = "forcePROD"
            $cfg_ImportRCFilePath = $env:IMPORT_RC_FILE_PATH
            $cfg_XSCInstallPath = $env:IMPORT_FILE_PATH
            $cfg_database = $env:DATABASE_NAME
            $cfg_server = $env:SERVER_DB
            $cfg_instance = $env:INSTANCE
            $cfg_XSCPassword = $env:XSC_PASSWORD
            $cfg_XSCUser = $env:XSC_USER
            $cfg_XSCUrl = $env:XSC_URL
            $cfg_XSCBUName = $env:XSC_BU_NAME
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> Start TR_RC import" > $cfg_logPath
            
            #wywo�anie importu trigger�w
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import trigger configuration" >> $cfg_logPath
            
            function importTrigger( $isDev )
            {
                Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import trigger" >> $cfg_logPath
            
                if ( $isDev )
                {
                    $path = $cfg_ImportRCFilePath + "DEV/" 
                }
                else
                {
                    $path = $cfg_ImportRCFilePath + "PROD/" 
                }    
                
                $dataImportPath = $cfg_XSCInstallPath + "Data/ClientsData/XSMS/bin"
                
                $getImportFileNames = Get-ChildItem $path* -Include *.xscsys, *.xml, *kfi  | sort Name -desc 
                
                foreach ( $file in $getImportFileNames )
                {
                    $unitName = $cfg_XSCBUName
                    $fileName = $file.name 
                    $fileFullName = $file.fullname
            
                    import -dataImportPath $dataImportPath -unitName $unitName -fileFullName $fileFullName 
                }
            }
            
            function import( $dataImportPath, $unitName, $fileFullName )
            {
                Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import" >> $cfg_logPath
            
                Write-Output "\$dataImportPath/DataImport -d \$unitName -a \$cfg_XSCUrl/blackbox/shippingmanagement.svc -f \$fileFullName -p \$cfg_XSCPassword -u \$cfg_XSCUser"
                & \$dataImportPath/DataImport -d \$unitName -a \$cfg_XSCUrl/blackbox/shippingmanagement.svc -f \$fileFullName -p \$cfg_XSCPassword -u \$cfg_XSCUser
            
                # odczytanie kodu
                # jeśli OK, to przeniesienie zaimportowanych plików do folderu Done + dodanie do nazwy pliku daty importu
                if ( $LASTEXITCODE -eq 0 ) 
                {
                    $importSuccessfuly = $true
            
                    $destinationPath = $path + "Done/" + $(Get-Date -format yyyyMMdd%Hmm) + "_" + $file.name
                        
                    # dodanie wpisu do logu co zostało zaimportowane
                    Write-Output "[INFO]: \$(Get-Date -format 'g') -> Imported $file" >> $cfg_logPath
                }
                else 
                {
                    $destinationPath = $path + "Failed/" + $(Get-Date -format yyyyMMdd%Hmm) + "_" + $file.name
                        
                    # dodanie wpisu do logu co zostało NIE zaimportowane
                    Write-Output "[ERR ]: \$(Get-Date -format 'g') -> Import $file failed" >> $cfg_logPath
                }
            }
            function TR_RC_activate
            {   
                $sqlQuery = "UPDATE [\$cfg_database].[dbo].[TB_EVLS_TRIGGER] SET TRIGGER_ACTIVE = 1  WHERE TRIGGER_ACTIVE = 0  AND TRIGGER_NAME LIKE 'TR_RC_%'  AND TRIGGER_DATA_TYPE = 'Routing';"
                
                try
                {
                    # Create the connection string
                    $sqlConnectionString ="Server = \$cfg_server/\$cfg_instance; Database = \$cfg_database; Integrated Security = True"
                    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection
                    $sqlConnection.ConnectionString = $sqlConnectionString
            
                    #Create the SQL Command object
                    $sqlCmd = New-Object System.Data.SqlClient.SqlCommand
                    $sqlCmd.CommandText = $sqlQuery
                    $sqlCmd.Connection = $SqlConnection
            
                    #Open SQL connection
                    $sqlCmd.Connection.Open()
            
                    #Execute the Query
                    $result = $sqlCmd.ExecuteNonQuery()
            
                    $sqlCmd.Connection.Close()
                }
                catch [Exception]
                {
                    Write-Output "[ERR ]: \$(Get-Date -format 'g') -> Trigger activation failed: " + $_.Exception.Message + "." > $cfg_logPath
                    $result = 0
                }
                return $result
            }
            
            if ( $forceDEV -eq "forceDEV" ) 
            {
                Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import DEV triggers" >> $cfg_logPath
                importTrigger( $true )
            }
            else 
            {
                Write-Output "[INFO]: \$(Get-Date -format 'g') -> Import PROD triggers" >> $cfg_logPath
                importTrigger( $false ) 
            }
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> Trigger activation" >> $cfg_logPath
            $trRCActivateResult = TR_RC_activate
            
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> $trRCActivateResult triggers was activated" >> $cfg_logPath
            Write-Output "[INFO]: \$(Get-Date -format 'g') -> End TR_RC import" >> $cfg_logPath

            '''.stripIndent())
    }
}