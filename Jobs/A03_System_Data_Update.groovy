freeStyleJob("Jobs/A03_System_Data_Update") {

    displayName('A03_System_Data_Update')
    description('Copies Routing Codes to the specified path')

	logRotator {
        daysToKeep(10)
    }
	
    label('DB_SQL2012')
  
       steps {
        powerShell ('''			
            #Copy System Data
            Write-Output "[INFO]: $(Get-Date -format \'g\') -> Start System Data copying" > $cfg_logPath

            Copy-Item \\\\192.168.200.111\\XLFiles\\RoutingCodes\\TEST\\* -destination "D:\\Data\\tst.carrier_modules.SQL2008.mpm.metapack.com\\Carrier System Data\\input" -recurse -force -Verbose

            Write-Output "[INFO]: $(Get-Date -format \'g\') -> Stop System Data copying" >> $cfg_logPath

            '''.stripIndent())
    }
}