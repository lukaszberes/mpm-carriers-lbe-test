freeStyleJob("Jobs/A07a_DB_Backup") {

    displayName('A07a_DB_Backup')
    description('Restores the Carrier_Modules database')

	logRotator {
        daysToKeep(30)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
            $cfg_repoPath = $env:PATH_DB_BACKUP
            $cfg_server = $env:SERVER_IP
            $cfg_instance = $env:INSTANCE
            $cfg_database = $env:DATABASE_NAME

            Write-Output "[INFO]: $(Get-Date -format \'g\') -> Start process" > $cfg_logPath

            function getInstalledVersion( $server, $instance, $database ) 
            {
                $getInstalledVersion = invoke-sqlcmd -Query "SELECT SYSTEM_VERSION FROM [CARRIER_MODULES].[dbo].[TB_EVLS_SYSTEM];" -ServerInstance "$server\\$instance" -QueryTimeout 0 -Verbose
                $installedVersion = $getInstalledVersion.SYSTEM_VERSION

                return $installedVersion
            }

            $actualyInstalledVersion = getInstalledVersion -server $cfg_server -instance $cfg_instance -database $cfg_database
            Write-Output "[INFO]: $(Get-Date -format \'g\') -> Installed XSC version is: $actualyInstalledVersion" >> $cfg_logPath

            if ( $actualyInstalledVersion -eq $null ) {
                Write-Output "[ERR ]: $(Get-Date -format \'g\') -> Can\'t get installed version (database connection problem?)" >> $cfg_logPath
                exit 9
            }

            Write-Output "[INFO]: $(Get-Date -format \'g\') -> Starting database backup" >> $cfg_logPath

            $backupFullPathName = $cfg_repoPath + "XSC_" + $actualyInstalledVersion + ".bak"
            function backup( $server, $instance, $database, $backupPath )
            {    
                try 
                {
                    $error.Clear()
                    $result = Backup-SqlDatabase -ServerInstance "$server\\$instance" -Database $database -CompressionOption On -BackupFile $backupPath  -Verbose
                    Write-Output $result
                } 
                catch [Exception] 
                {
                    $result = $error
                }
                return $result
            }

            $backupResult = backup -server $cfg_server -instance $cfg_instance -database $cfg_database -backupPath $backupFullPathName

            Write-Output $backupResult

            '''.stripIndent())
    }
}