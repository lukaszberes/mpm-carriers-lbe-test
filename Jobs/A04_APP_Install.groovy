freeStyleJob("Jobs/A04_APP_Install") {

    displayName('A04_APP_Install')
    description('Application installation')

	logRotator {
        daysToKeep(30)
    }
	
    label('DB_SQL2012')
  
    steps {
        powerShell ('''			
          	[Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')

			$strString = "------------------------------------------------------------"
			$jobName = $ENV:JOB_NAME
			$environment = "SQL2008"
			$webclient = New-Object System.Net.WebClient
			$urlS3 = „https://s3-eu-west-1.amazonaws.com/staging-eu-west-1-mpm-builds/XShippingCenter_„
			$file = „d:\\Apps\\tst.carrier_modules.SQL2008.mpm.metapack.com\\Data\\Updates\\”
			$XSCUpdateFilePath = „https://s3-eu-west-1.amazonaws.com/staging-eu-west-1-mpm-builds/XShippingCenter_LATEST.zip„
			#$XSCUpdateFilePath = „http://staging-eu-west-1-mpm-builds.s3.amazonaws.com/XShippingCenter_2.19.41.4226.zip„
			$XSCLocalUpdatePath = „d:\\Apps\\tst.carrier_modules.SQL2008.mpm.metapack.com\\Data\\Updates\\XShippingCenter_LATEST.zip”

			#DEBUG ON or OFF
			$DebugPreference = "SilentlyContinue" # "Continue" or "SilentlyContinue"
			$importSuccessfuly = $false

			# zmienne z prefiksem cfg_ sa zdefiniowane w c:\\programs\\jenkins\\script\\All_Modules_Config.ps1
			# sa do zmienne do wykorzystania w wielu skryptach, ale w kazdym mozna je nadpisac
			. c:\\programs\\jenkins\\script\\All_Modules_Config.ps1  $jobName $environment #test SQL2008
			. c:\\programs\\jenkins\\script\\Library\\Calendar_functions.ps1
			. c:\\programs\\jenkins\\script\\Library\\Tools_functions.ps1

			Write-Output "Test: ${env.VERSION}"

			if ( ! ( Test-Path $cfg_logDir ) ) { mkdir $cfg_logDir }

			Write-Output "[INFO]: $(Get-Date -format 'g') -> Start application install" > $cfg_logPath

			# odczytanie informacji jak wersja publiczna byla ostatnio instalowana
			# findStringInFile() is function in Tools_functions.ps1
			$previousInstalledPubVersion = findStringInFile -file $cfg_exchangeInfoFile  -pattern "version:" 
			$previousInstalledPubVersion = $previousInstalledPubVersion.Line
			$previousInstalledPubVersion = $previousInstalledPubVersion.Substring( $previousInstalledPubVersion.IndexOf(":") + 1 )

			# isEndOfSprint() is function in Calendar_functions.ps1


			Write-Output "[INFO]: $(Get-Date -format 'g') -> Is end of sprint: $( isEndOfSprint )" >> $cfg_logPath

			if ( isEndOfSprint  )
			{
			    <#if (Test-Path $cfg_newPubVersionPath)
			    {
			        # getLastXSCVersion() is function in Tools_functions.ps1
			        $lastPubVersion = getLastXSCVersion -path $cfg_newPubVersionPath
			        Write-Output "[INFO]: $(Get-Date -format 'g') -> Last published XSC version is: $lastPubVersion" >> $cfg_logPath
			    }
			    else
			    {
			        Write-Output "[ERR ]: $(Get-Date -format 'g') -> Missing $cfg_newVersionFormat or $cfg_oldVersionFormat directories" >> $cfg_logPath
			        exit 9
			    }#>

			    # getInstalledVersion() is function in Tools_functions.ps1
			    #$restoredVersion = getInstalledVersion -path $cfg_XSCInstallPath
			    Write-Output "[INFO]: $(Get-Date -format 'g') -> Previous installed XSC version is: $previousInstalledPubVersion" >> $cfg_logPath

			    # compareVersion() is function in Tools_functions.ps1

				$lastPubVersionFullPath = getLastXSCVersionFullName -path $cfg_newPubVersionPath
				#$XSCUpdateFilePath = $cfg_XSCDevPath + $lastDevVersionFullPath 
				#$XSCLocalUpdatePath = $cfg_XSCInstallPath + "Data\\Updates\\"
				#$XSCUpdateFilePath = $urlS3 + $lastDevVersion + ".zip"
				#$XSCLocalUpdatePath = $cfg_XSCInstallPath + "Data\\Updates\\XShippingCenter_" + $lastDevVersion + ".zip"

				try
				{
					Write-Output "[INFO]: $(Get-Date -format 'g') -> Copying $XSCUpdateFilePath to $XSCLocalUpdatePath" >> $cfg_logPath
					#Copy-Item $XSCUpdateFilePath $XSCLocalUpdatePath #komenda która po url zapisuje plik pod opowoednia nazwa

					#Dodane przez LBE
					write-host $strString
					write-host "Downloading the installation package after publication"
					write-host "Dowlnload from: " + $XSCUpdateFilePath
					write-host "Dowlnload to: " + $XSCLocalUpdatePath
					write-host $strString
					write-host $lastPubVersion
					$webclient.DownloadFile($XSCUpdateFilePath,$XSCLocalUpdatePath)

			                $packageFile = [IO.Compression.ZipFile]::OpenRead($XSCLocalUpdatePath).Entries | Select-Object -first 1
			                $packageVersion = $packageFile.FullName -split "XShippingCenter_(.*)/"
					Write-Host "Package version: " $packageVersion
			                Write-Output "$packageVersion" > $ENV:WORKSPACE\\packageVersion.txt

					#rozpakowanie paczki
					<#
					$dir = 'd:\\Apps\\tst.carrier_modules.SQL2008.mpm.metapack.com\\Data\\Updates\\\'
					$ns = 'System.IO.Compression.FileSystem'
					[Reflection.Assembly]::LoadWithPartialName($ns)
					[IO.Compression.ZipFile]::ExtractToDirectory($XSCLocalUpdatePath, $dir)
					#>
				}
				catch [Exception]
				{
					Write-Output "[ERR ]: $(Get-Date -format 'g') -> $_.Exception.Message " >> $cfg_logPath
					exit 9
				}

				Write-Output "[INFO]: $(Get-Date -format 'g') -> Installing $lastPubVersion version" >> $cfg_logPath

				$DTPart = $cfg_XSCInstallPath + "XSCDeploymentTool\\bin\\mpmdt.exe"
				Write-Output "[INFO]: $(Get-Date -format 'g') -> $DTPart" >> $cfg_logPath

				& $DTPart i

				<#if ( $LASTEXITCODE -ne 0 ) 
				{
					Write-Output "[INFO]: $(Get-Date -format 'g') ->  Last exitcode from mpmdt.exe" + $LASTEXITCODE >> $cfg_logPath
					exit 9
				}#>

				$installedVersion = $lastPubVersion

				# wpisanie do pliku wymiany info numer instalowanej wersji
				# \\\\192.168.200.111\\Exchange\\Development\\AllModules\\GoExchangeFiles\\exchangeInfo.txt
				$lastPubVersion = $lastPubVersion.Substring( $lastPubVersion.IndexOf("_") + 1 )
				$lastPubVersion = $lastPubVersion.Substring( 0, $lastPubVersion.IndexOf("\\") )
				Write-Output "version:$lastPubVersion" > $cfg_exchangeInfoFile

			}
			else
			{
			    if (Test-Path $cfg_newDevVersionPath)
			    {
			        # getLastXSCVersion() is function in Tools_functions.ps1
			        $lastDevVersion = getLastXSCVersion -path $cfg_newDevVersionPath
			        Write-Output "[INFO]: $(Get-Date -format 'g') -> Last published development XSC version is: $lastDevVersion" >> $cfg_logPath
			    }
			    else
			    {
			        Write-Output "[ERR ]: $(Get-Date -format 'g') -> Missing $cfg_newVersionFormat or $cfg_oldVersionFormat directories" >> $cfg_logPath
			        exit 9
			    }		

			    #getInstalledVersion() is function in Tools_functions.ps1
			    #$restoredVersion = getInstalledVersion -path $cfg_XSCInstallPath
			    #Write-Output "[INFO]: $(Get-Date -format 'g') -> Installed XSC version is: $restoredVersion" >> $cfg_logPath

			    # compareVersion() is function in Tools_functions.ps1

				$lastDevVersionFullPath = getLastXSCVersionFullName -path $cfg_newDevVersionPath
				#$XSCUpdateFilePath = $cfg_XSCDevPath + $lastDevVersionFullPath 
				#$XSCLocalUpdatePath = $cfg_XSCInstallPath + "Data\\Updates\\"
				#$XSCUpdateFilePath = $urlS3 + $lastDevVersion + ".zip"
				#$XSCLocalUpdatePath = $cfg_XSCInstallPath + "Data\\Updates\\XShippingCenter_" + $lastDevVersion + ".zip"



				try 
				{
					Write-Output "[INFO]: $(Get-Date -format 'g') -> Copying $XSCUpdateFilePath to $XSCLocalUpdatePath" >> $cfg_logPath
					#Copy-Item $XSCUpdateFilePath $XSCLocalUpdatePath #komenda która po url zapisuje plik pod opowoednia nazwa

					#Dodane przez LBE
					write-host $strString
					write-host "Downloading the installation package during the sprint"
					write-host "Dowlnload from: " + $XSCUpdateFilePath
					write-host "Dowlnload to: " + $XSCLocalUpdatePath
					write-host "lastPubVersion: " $lastPubVersion
					write-host "lastDevVersion: " $lastDevVersion
					write-host $strString
					$webclient.DownloadFile($XSCUpdateFilePath,$XSCLocalUpdatePath)

			                $packageFile = [IO.Compression.ZipFile]::OpenRead($XSCLocalUpdatePath).Entries | Select-Object -first 1
			                $packageVersion = $packageFile.FullName -split "XShippingCenter_(.*)/"
			                Write-Output "$packageVersion" > $ENV:WORKSPACE\\packageVersion.txt
					Write-Host "Package version: " $packageVersion

					#rozpakowanie paczki
					<#
					$dir = 'd:\\Apps\\tst.carrier_modules.SQL2008.mpm.metapack.com\\Data\\Updates\\\'
					$ns = 'System.IO.Compression.FileSystem'
					[Reflection.Assembly]::LoadWithPartialName($ns)
					[IO.Compression.ZipFile]::ExtractToDirectory($XSCLocalUpdatePath, $dir)
					#>

				}
				catch [Exception]
				{
					Write-Output "[ERR ]: $(Get-Date -format 'g') -> $_.Exception.Message " >> $cfg_logPath
					exit 9
				}

				Write-Output "[INFO]: $(Get-Date -format 'g') -> Installing $lastDevVersion version" >> $cfg_logPath

				$DTPart = $cfg_XSCInstallPath + "XSCDeploymentTool\\bin\\mpmdt.exe"
				Write-Output "[INFO]: $(Get-Date -format 'g') -> $DTPart" >> $cfg_logPath

				& $DTPart i

				<#if ( $LASTEXITCODE -ne 0 ) 
				{
					Write-Output "[INFO]: $(Get-Date -format 'g') ->  Last exitcode from mpmdt.exe" + $LASTEXITCODE >> $cfg_logPath
					exit 9
				}#>

				#$DTPart = $cfg_XSCInstallPath + "runme.exe" + " " + $cfg_XSCInstallPath + "XSCDeploymentTool\\bin\\MPMDeploymentTool.exe"
				#Write-Output "[INFO]: $(Get-Date -format 'g') -> $DTPart" >> $cfg_logPath
				#& $DTPart

				$installedVersion = $lastDevVersion
			}
			echo (Get-ItemProperty "D:\\Apps\\tst.carrier_modules.SQL2008.mpm.metapack.com\\MPM Deployment Tool.exe").VersionInfo.ProductVersion > $ENV:WORKSPACE\\last_version.txt
			#zabicie procesu w3wp co by nie zajmowa³ 100% zajêtopœci procesora
			Write-Output "[INFO]: $(Get-Date -format 'g') -> Kill w3wp process" >> $cfg_logPath
			#Stop-Process -Name w3wp -Force -Verbose
			$code = [scriptblock]::Create("Stop-Process -Name w3wp -Force -Verbose")
			# zatrzymanie us³ugi jako Administrator
			# polecenie zabicia procesu zdefiniowane jako scriptblock powy¿ej
			Start-Process -FilePath powershell.exe -ArgumentList $code -verb RunAs


			#Start Guard - 30-09-2015 przeniesione do osbnego skryptu Guard_Start.ps1. WYkorzystywany przez osobny job w Jenkins
			####Write-Output "[INFO]: $(Get-Date -format 'g') -> Starting XSCGuard: $cfg_XGuardServiceName" >> $cfg_logPath
			####Start-Service $cfg_XGuardServiceName

			#wys³anie requesta aby "obudzic" IIS po instalacji nowerj werscji XSC
			Write-Output "[INFO]: $(Get-Date -format 'g') -> Send request to $cfg_XSCUrl/blackbox/shippingmanagement.svc" >> $cfg_logPath
			if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
			{
			$certCallback = @"
			    using System;
			    using System.Net;
			    using System.Net.Security;
			    using System.Security.Cryptography.X509Certificates;
			    public class ServerCertificateValidationCallback
			    {
			        public static void Ignore()
			        {
			            if(ServicePointManager.ServerCertificateValidationCallback ==null)
			            {
			                ServicePointManager.ServerCertificateValidationCallback += 
			                    delegate
			                    (
			                        Object obj, 
			                        X509Certificate certificate, 
			                        X509Chain chain, 
			                        SslPolicyErrors errors
			                    )
			                    {
			                        return true;
			                    };
			            }
			        }
			    }
			"@
			    Add-Type $certCallback
			 }
			[ServerCertificateValidationCallback]::Ignore()

			Invoke-WebRequest -URI $cfg_XSCUrl/blackbox/blackbox.svc -UseBasicParsing
			Invoke-WebRequest -URI $cfg_XSCUrl/blackbox/shippingmanagement.svc -UseBasicParsing

			Write-Output "[INFO]: $(Get-Date -format 'g') -> End application install" >> $cfg_logPath

            '''.stripIndent())
    }
}